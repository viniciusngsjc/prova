<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LivrosController extends Controller
{
    public function index()
    {

        return view ("livros.index");
    }

    public function create()
    {
        return view ("livros.create");
    }

    public function store(Request $request)
    {

        //$dt_nasc = $this->formata_data($request->dt_nasc);

        DB::table('livros')->insert([
            'titulo'=> $request->titulo,
            'nome_editora'=>$request->nome_editora,
            'tipo_livro'=>$request->tipo_livro,
            'emprestado'=>$request->emprestado
        ]);

        //Agenda::create($request->all());

        return redirect()->route('livros.index');
    }

}
